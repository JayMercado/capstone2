let profileContainer = document.querySelector('#profileContainer');

let token = localStorage.getItem("token");
let enrollmentsHTML;

fetch('https://salty-earth-35039.herokuapp.com/api/users/details', {
    // method: 'GET', can be omitted if GET request
    headers: {
        "Authorization": `Bearer ${token}`
    }
})
    .then(res => res.json())
    .then(data => {
        profileContainer.innerHTML = `
        <div class="col-md-12">
            <div class="jumbotron">
                <div class="text-center">
                    <h2>Name: ${data.firstName} ${data.lastName}</h2>
                    <h2>Phone Number: ${data.mobileNo} </h2>
                    <h2>Email: ${data.email}</h2>
                </div>
                <h5>Class History</h5>
                <hr class="my-4">
                <div class="container">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Course</th>
                                <th scope="col">Enrolled On</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        `
        renderCourses(data)
    })

async function renderCourses(data) {
    let enrollments = data.enrollments;
    let coursesTbody = document.querySelector('tbody');
    enrollmentsHtml = '';
    for (let enrollment of enrollments) {
        let courseDetails = await getCourseDetails(enrollment.courseId);

        let date = new Date(enrollment.enrolledOn);

        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }

        let formattedDate = day + '-' + month + '-' + year;

        enrollmentsHtml += `
            <tr>
                <td id="course-name">${courseDetails.name}</td>
                <td>${formattedDate}</td>
                <td>Enrolled</td>
            </tr>`
    }

    coursesTbody.innerHTML = enrollmentsHtml;
}


async function getCourseDetails(courseId) {
    let response = await fetch(`https://salty-earth-35039.herokuapp.com/api/courses/${courseId}`);
    return await response.json();
}










//let token = localStorage.getItem("token");
// let profileContainer = document.querySelector('#profileContainer');

// if (!token || token === null) {
//     alert('You must login first');
//     window.location.replace("./login.html");
// } else {
//     fetch('https://salty-earth-35039.herokuapp.com/api/users/details', {
//         headers: {
//             "Authorization": `Bearer ${token}`
//         }
//     })
//         .then(res => res.json())
//         .then(data => {
//             profileContainer.innerHTML = 
//             `
//             <div class="col-md-12">
// 				<section class="jumbotron my-5">		
// 					<h3 class="text-center">First Name: ${data.firstName}</h3>
// 					<h3 class="text-center">Last Name: ${data.lastName}</h3>
// 					<h3 class="text-center">Email: ${data.email}</h3>
// 					<h3 class="text-center">Email: ${data.mobileNo}</h3>
// 					<h3 class="mt-5">Class History</h3>
// 					<table class="table">
// 						<thead>
// 							<tr>
// 								<th> Course ID </th>
// 								<th> Enrolled On </th>
// 								<th> Status </th>
// 							</tr>
//                         </thead>
//                         <tbody id="coursesContainer">
//                         </tbody>
// 					</table> 

// 				</section>
// 			</div>

//             `
//             coursesContainer = document.querySelector('#coursesContainer');
//             data.enrollments.forEach(enrollment => {

//                 fetch(`http://localhost:4000/api/courses/${enrollment.courseId}`)
//                 .then(res => res.json())
//                 .then(data => {
//                     coursesContainer.innerHTML =
//                     `
//                         <tr>
//                             <td>${data.name}</td>
//                             <td>${enrollment.enrolledOn}</td>
//                             <td>${enrollment.status}</td>
//                         <tr>
//                     `
//                 })
//             })
//         })
// }