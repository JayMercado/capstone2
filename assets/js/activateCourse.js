let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://salty-earth-35039.herokuapp.com/api/courses/${courseId}`, {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
})
    .then(res => res.json())
    .then(data => {
        if (data === true) {
            alert("Course activated!");
            window.location.replace("courses.html");
        } else {
            alert("Something went wrong.")
        }
    }) 