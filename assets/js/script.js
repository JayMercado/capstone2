let nav = document.querySelector(".navbar-nav");

// localStorage = an object used to store information in our clients/ devices
let userToken = localStorage.getItem("token");

if (!userToken) {
    nav.innerHTML +=
        `
            <li class="nav-item">
                <a href="./login.html" class="nav-link">
                    Login
                </a>            
            </li>
        `
} else {
    nav.innerHTML +=
        `
        <li class="nav-item">
            <a href="./profile.html" class="nav-link"> Profile </a>
        </li>

        <li class="nav-item">
            <a href="./logout.html" class="nav-link">
                Logout
            </a>
        </li>
        `
}
