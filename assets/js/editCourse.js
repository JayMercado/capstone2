let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

let editForm = document.querySelector('#editCourse');


editForm.addEventListener("submit", (e) => {

    e.preventDefault();

    let courseName = document.querySelector('#courseName').value;
    let courseDescription = document.querySelector('#courseDescription').value;
    let coursePrice = document.querySelector('#coursePrice').value;

    let token = localStorage.getItem("token");
    if (courseName !== '' && courseDescription !== '' && coursePrice !== '') {
        fetch('https://salty-earth-35039.herokuapp.com/api/courses/', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId,
                name: courseName,
                description: courseDescription,
                price: coursePrice
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data === true) {
                    alert("Course updated!");
                    window.location.replace("courses.html");
                } else {
                    alert("Something went wrong.")
                }
                window.location.replace('courses.html');
            })
    } else {
        alert("Something went wrong.");
    }
})