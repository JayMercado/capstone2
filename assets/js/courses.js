// Retrieve the user information for isAdmin
let adminUser = localStorage.getItem("isAdmin");

// will contain the html for the different butoons per user
let cardFooter;

let modalButton = document.querySelector('#adminButton');

fetch('https://salty-earth-35039.herokuapp.com/api/courses')
    .then(res => res.json())
    .then(data => {
        
        // Active courses
        let activeCourses = data.filter(course => course.isActive === true);

        // Variable to store the card/ message to show if there's no courses
        let courseData;

        let activeCourseData;

        if (data.length < 1) {
            courseData = "No courses available";
        } else {
            if (adminUser === 'false' || !adminUser) {
                activeCourseData = activeCourses.map(course => {
                        cardFooter =
                            `
                        <a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
                        `;
                    return (` 
                <div class="col-md-6 my-3">
                    <div class='card'>
                        <div class='card-body'>
                            <h5 class='card-title'>${course.name}</h5>
                            <p class='card-text text-left'>
                                ${course.description}
                            </p>
                            <p class='card-text text-right'>
                                ₱ ${course.price}
                            </p>
    
                        </div>
                        <div class='card-footer'>
                            ${cardFooter}
                        </div>
                    </div>
                </div> 
                `)
                }).join("")

                let coursesContainer = document.querySelector("#coursesContainer");

                coursesContainer.innerHTML = activeCourseData;
                modalButton.innerHTML = null;
            } else {
                courseData = data.map(course => {
                    cardFooter =
                            `
                    <a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> View Course </a>
                    <a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>                    
                    `;
                    if (course.isActive === true) {
                        cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>`
                    } else {
                        cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-success text-white btn-block successButton"> Activate Course </a>`;
                    }
                    return (` 
                <div class="col-md-6 my-3">
                    <div class='card'>
                        <div class='card-body'>
                            <h5 class='card-title'>${course.name}</h5>
                            <p class='card-text text-left'>
                                ${course.description}
                            </p>
                            <p class='card-text text-right'>
                                ₱ ${course.price}
                            </p>
    
                        </div>
                        <div class='card-footer'>
                            ${cardFooter}
                        </div>
                    </div>
                </div> 
                `)
                }).join("")

                let coursesContainer = document.querySelector("#coursesContainer");

                coursesContainer.innerHTML = courseData;

                modalButton.innerHTML =
                `
                <div class="col-md-2 offset-md-10">
                    <a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
                </div>
                `
            }
        }
    });