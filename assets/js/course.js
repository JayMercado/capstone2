// instantiate a URLSearchParams object so we can execute methods to access the parameters 
let params = new URLSearchParams(window.location.search);

// console.log(params);
// console.log(params.has('courseId'));
// console.log(params.get('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

let token = localStorage.getItem('token');
let adminUser = localStorage.getItem("isAdmin");


if (adminUser === 'false' || !adminUser) {
    fetch(`https://salty-earth-35039.herokuapp.com/api/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            courseName.innerHTML = data.name;
            courseDesc.innerHTML = data.description;
            coursePrice.innerHTML = data.price;
            enrollContainer.innerHTML =
                `
    <button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
    `
            document.querySelector("#enrollButton").addEventListener("click", () => {
                fetch('https://salty-earth-35039.herokuapp.com/api/users/enroll', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        courseId: courseId
                    })
                })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);
                        if (data === true) {
                            alert("Thank you for enrolling! See you in class!");
                            window.location.replace("courses.html");
                        } else {
                            alert("Something went wrong.")
                        }
                    })
            })
        })
} else if (adminUser === 'true' || adminUser) {
    fetch(`https://salty-earth-35039.herokuapp.com/api/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            courseName.innerHTML = data.name;
            courseDesc.innerHTML = data.description;
            coursePrice.innerHTML = data.price;

            let jumbotron = document.querySelector('.jumbotron');
            jumbotron.innerHTML += `
                <h5 style="text-align: justify">Enrollees</h5>
                <hr class="my-4">
                <ul id="enrolleesList" style="text-align: justify"></ul>
                `;

            
            let enrolleesList = document.querySelector('#enrolleesList');
            let enrollees = data.enrollees;
            enrollees.forEach(enrollee => {
                fetch(`https://salty-earth-35039.herokuapp.com/api/users/${enrollee.userId}`)
                    .then(res => res.json())
                    .then(data => {
                        let listChild = document.createElement('li');
                        listChild.innerHTML = `${data.firstName} ${data.lastName}`;
                        enrolleesList.appendChild(listChild);
                    })
            })
            //renderEnrollees(data);
        })
}

// async function renderEnrollees(data) {
//     let enrollees = data.enrollees;
//     let enrolleesList = document.querySelector('#enrolleesList');

//     enrolleesHtml = '';

//     for (let enrollee of enrollees) {
//         let enrolleeDetails = await getEnrolleeDetails(enrollee.userId);
//         if(Object.keys(enrolleeDetails).length > 0) {
//             enrolleesHtml += `<li>${enrolleeDetails.firstName} ${enrolleeDetails.lastName}</li>`
//         }

//     }

//     console.log("Hello World");
//     enrolleesList.innerHTML = enrolleesHtml;
// }


// async function getEnrolleeDetails(userId) {
//     let response = await fetch(`http://localhost:4000/api/users/${userId}`)
//     return await response.json();
// }