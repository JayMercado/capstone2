let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {
    // prevents the form to revert to its defaulrt/ blank values
    e.preventDefault();

    let firstName = document.querySelector('#firstName').value;
    let lastName = document.querySelector('#lastName').value;
    let mobileNo = document.querySelector('#mobileNumber').value;
    let email = document.querySelector('#userEmail').value;
    let password = document.querySelector('#password1').value;
    let password2 = document.querySelector('#password2').value;

    if ((password !== '' && password2 !== '') && (password === password2) && (mobileNo.length === 11)) {

        //fetch('url', {options})
        // to process a request
        fetch('https://salty-earth-35039.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email
            })
        })
            .then(res => res.json())
            .then(data => {
                
                console.log(data);

                if (data === false) {
                    fetch('https://salty-earth-35039.herokuapp.com/api/users/', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName,
                            lastName,
                            email,
                            password,
                            mobileNo
                        })
                    })
                    .then(res => res.json())
                    .then(data => {

                        console.log(data);

                        if(data === true) {
                            alert("registered successfully");
                            window.location.replace("login.html")
                        } else {
                            alert("something went wrong");
                        }
                    })
                } else {
                    alert("Email has already been used. Try a different one.")
                }
            })

    } else {
        alert("Something went wrong.");
    }
})