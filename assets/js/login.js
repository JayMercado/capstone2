let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener("submit", (e) => {

    e.preventDefault();

    let email = document.querySelector('#userEmail').value;
    let password = document.querySelector('#password').value;

    console.log(email, password);
    if(email == '' || password == '') {
        alert("Please input your email or password");
    } else {
        fetch('https://salty-earth-35039.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data.accessToken) {
                //store the token in local storage
                localStorage.setItem('token', data.accessToken);
                fetch('https://salty-earth-35039.herokuapp.com/api/users/details', {
                    // method: 'GET', can be omitted if GET request
                    headers: {
                        "Authorization": `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    // Store user details in the local storage for future use
                    localStorage.setItem('id', data._id);
                    localStorage.setItem('isAdmin', data.isAdmin);

                    // redirect user to our courses page
                    window.location.replace("courses.html");
                })
            } else {
                alert("Something went wrong");
            }
        })
    }
})